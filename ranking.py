SUBJECTS = 3

def parse_line(line: str) -> list:
    name, *marks = line.strip().split()
    marks = [int(_) for _ in marks]
    return [name, marks]

def collect_data():
    return [parse_line(line) for line in open("studentRanking.txt")]

STUDENT_RECORD = collect_data()

def compress(s: str) -> str:
    if len(s) == 1:
        return s
    else:
        return s[0] + compress(s[1:]) if s[0] != s[1] else compress(s[1:])

def rel(a, b):
    return 'H' if a > b else 'L'

class Student:
    def __init__(self, index):
        self.name = STUDENT_RECORD[index][0]
        self.marks = STUDENT_RECORD[index][1]

    def compare(student1, student2):
        return compress(''.join([rel(a, b) for a, b in zip(student1.marks, student2.marks)]))

def ranking():
    result = []
    i = 0
    j = 1
    for _ in STUDENT_RECORD:
        student1 = Student(i)
        student2 = Student(j)
        if Student.compare(student1, student2) == 'H':
            i += 1
            j += 1
            result.append(f'{student1.name} > {student2.name}')
        elif Student.compare(student1, student2) == 'L':
            j += 1
            result.append(f'{student2.name} > {student1.name}')
        else:
            j += 1
    return result

print(ranking())